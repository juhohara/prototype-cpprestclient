# About

Client application (Spring MVC) that will access via http to a [rest server fully implemented in C++ (using C++ REST SDK from Microsoft)](https://gitlab.com/juhohara/prototype-cpprestsdk).

Author: [Juliana Hohara](http://juhohara.gitlab.io/)

To run this project you must have:

* Installed Java 8 SDK.

**See the Markdown page in Eclipse**

* [Markdown Text Editor](https://marketplace.eclipse.org/content/markdown-text-editor)  

**Eclipse - How import a Maven Project?**

* File > Import... > Maven > Existing Maven Projects
* Select Maven Projects Window > Browse...(Root Directory) > click in 'Finish'

**Container suggestion**

- WildFly 10.1.0.Final
- JBoss Web deployment descriptor: WebContent/WEB-INF/jboss-web.xml

**Access via browser (local)**

- Page:[http://localhost:8080/cpprestclient/](http://localhost:8080/cpprestclient/)