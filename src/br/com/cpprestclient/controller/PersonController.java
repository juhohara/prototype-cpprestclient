package br.com.cpprestclient.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import br.com.cpprestclient.pojo.Person;

@Controller
public class PersonController {

	@RequestMapping("lista")
	public String lista(Model model) {
		
		Person p = new Person("Somebody");
		
		final String uri = "http://127.0.0.1:34568/";
		
	    RestTemplate restTemplate = new RestTemplate();
	    
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<Object> entity = new HttpEntity<Object>(p, headers);
	    	    
	    Object answer = restTemplate.postForObject(uri, entity, Object.class);
	    System.out.println("POST REST Server answer: " + answer);
	    	    		
	    List<Person> persons = new ArrayList<Person>();
		persons.add(p);	    
		model.addAttribute("persons", persons);
	    
		return "client/lista";
	}
}