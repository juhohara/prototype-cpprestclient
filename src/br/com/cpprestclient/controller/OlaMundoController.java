package br.com.cpprestclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class OlaMundoController {
	
	@RequestMapping("/olaMundoSpring")
	public String execute() {
		
		final String uri = "http://127.0.0.1:34568/";
		
	    RestTemplate restTemplate = new RestTemplate();
	    	    	    
	    String answer = restTemplate.getForObject(uri, String.class);
	    System.out.println("GET REST Server answer: " + answer);
	    		
		return "ok";
		
	}
	
}
