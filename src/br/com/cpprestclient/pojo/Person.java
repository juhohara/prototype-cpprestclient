package br.com.cpprestclient.pojo;

public class Person {
	
	private String m_name;
	
	public Person() {
		this.setM_name("Unknown");
	}
	
	public Person(String name) {
		this.setM_name(name);
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

}
