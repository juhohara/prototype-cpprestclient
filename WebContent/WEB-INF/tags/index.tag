<%@tag description="Index Page template" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="url" value="${pageContext.request.contextPath}"></c:set>
<t:genericpage>

    <jsp:attribute name="header">
      <h2>Prototype client side to test C++ REST SDK</h2>
	  <h3>Basic Spring MVC project with MAVEN, JavaScript, HTML and CSS.</h3>
	  <p>Prototype client side to test C++ REST SDK - codename Casablanca (Microsoft)</p>
	  <p>Images by Ansel Adams</p>
	  
	  <div class="menu">
	      <nav>
	          <a href="${url}">Home</a>
	          <a href="${url}/olaMundoSpring">GET Method</a>
	          <a href="${url}/lista">POST Method</a>
	      </nav>
      </div>
	  
    </jsp:attribute>    
    <jsp:attribute name="footer">
      <p>2017</p>
    </jsp:attribute>
    <jsp:body>
        <jsp:doBody/>
    </jsp:body>
    
</t:genericpage>