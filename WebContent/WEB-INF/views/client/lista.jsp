<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<t:index> 
   <jsp:body>
	  <div class="row">
		  <div class="column">
		  <img src="resources/images/AA01.jpg" title="Ansel Adams - Clearing Storm, Sonoma County (1951)">
		  </div>
		  <div class="column">
		  	<table>
				<tr>
					<th>Name</th>
				</tr>
				<c:forEach items="${persons}" var="person">			
					<tr id="person">
						<td>${person.m_name}</td>	
					</tr>				
				</c:forEach>
			</table>
		  </div>
	  </div>
   </jsp:body>	
</t:index>