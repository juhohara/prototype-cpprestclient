<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<t:index> 
   <jsp:body>
	  <div class="row">
		  <div class="column">
		  <img src="resources/images/AA03.jpg" title="Ansel Adams - Clearing Storm, Sonoma County (1951)">
		  </div>
		  <div class="column">
		  	<h2>Hello World with Spring MVC!</h2>
		  </div>
	  </div>
   </jsp:body>	
</t:index>