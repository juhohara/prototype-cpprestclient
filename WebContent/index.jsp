<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:index> 
   <jsp:body>
	<div class="row">
	  <div class="column">
	  <img src="resources/images/AA01.jpg" title="Ansel Adams - Clearing Storm, Sonoma County (1951)">
	  </div>
	  <div class="column">
	  <img src="resources/images/AA02.jpg" title="Ansel Adams - Moonrise, Hernandez (1941)">
	  </div>
	  <div class="column">
	  <img src="resources/images/AA03.jpg" title="Ansel Adams - Oak Tree, Sunset City (1963)">
	  </div>
	</div>
   </jsp:body>	
</t:index>
